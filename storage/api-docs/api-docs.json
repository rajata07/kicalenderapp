{
    "openapi": "3.0.0",
    "info": {
        "title": "KI Labs Calender API",
        "description": "KI Labs Calender API documentation",
        "termsOfService": "http://swagger.io/terms/",
        "contact": {
            "email": "rajata07@gmail.com"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "1.0.0"
    },
    "paths": {
        "/api/time-slots/": {
            "post": {
                "tags": [
                    "Time Slot"
                ],
                "summary": "Save available time slots of given user",
                "operationId": "create",
                "requestBody": {
                    "description": "Create Time slot object",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/TimeSlot"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/TimeSlot"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/api/time-slots/fetch/": {
            "post": {
                "tags": [
                    "Time Slot"
                ],
                "summary": "Fetch a collection of time slots for given users",
                "operationId": "fetch",
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "properties": {
                                    "users": {
                                        "type": "string"
                                    }
                                },
                                "example": {
                                    "users": [
                                        "arora18@gmail.com",
                                        "rajata07@gmail.com"
                                    ]
                                }
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "successful operation",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "properties": {
                                        "time_slots": {
                                            "type": "string"
                                        },
                                        "role": {
                                            "type": "string"
                                        },
                                        "email": {
                                            "type": "string"
                                        },
                                        "name": {
                                            "type": "string"
                                        }
                                    },
                                    "example": {
                                        "time_slots": "",
                                        "email": "",
                                        "name": "",
                                        "role": ""
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/api/user/{id}": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "Fetch user by id",
                "description": "Fetch user data for given id",
                "operationId": "fetch",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "ID of user to fetch",
                        "required": true,
                        "schema": {
                            "type": "integer"
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "In case of successful operation, Response contains user data otherwise corresponding errors are thrown",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/User"
                                }
                            }
                        }
                    }
                }
            },
            "delete": {
                "tags": [
                    "User"
                ],
                "summary": "Delete user",
                "description": "Removes the user data.",
                "operationId": "delete",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "description": "The user id that needs to be deleted",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "User deleted"
                    },
                    "404": {
                        "description": "User not found"
                    }
                }
            }
        },
        "/api/user/": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "Create user",
                "description": "Simply creates a new user without any auth required",
                "operationId": "createUser",
                "requestBody": {
                    "description": "Create user object",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/User"
                            }
                        }
                    }
                },
                "responses": {
                    "201": {
                        "description": "In case of successful operation, Response contains user data otherwise corresponding errors are thrown",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/User"
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "components": {
        "schemas": {
            "TimeSlot": {
                "required": [
                    "user_id",
                    "time_slots"
                ],
                "properties": {
                    "user_id": {
                        "description": "(Id of the user)",
                        "type": "integer"
                    },
                    "time_slots": {
                        "description": "(Slots available for user as json)",
                        "type": "string"
                    },
                    "email": {
                        "description": "Email of the user",
                        "type": "string"
                    }
                },
                "type": "object",
                "example": {
                    "user_id": 10,
                    "time_slots": [
                        {
                            "start_time": "2018-05-20 09:00",
                            "end_time": "2018-05-20 16:00"
                        },
                        {
                            "start_time": "2018-05-20 10:00",
                            "end_time": "2018-05-20 11:00"
                        }
                    ]
                }
            },
            "User": {
                "required": [
                    "email",
                    "role"
                ],
                "properties": {
                    "role": {
                        "description": "Role of the user (interviewer or candidate)",
                        "type": "string"
                    },
                    "name": {
                        "description": "Name the user (optional)",
                        "type": "string"
                    },
                    "email": {
                        "description": "Email of the user",
                        "type": "string"
                    }
                },
                "type": "object",
                "example": {
                    "role": "interviewer",
                    "name": "Rajat",
                    "email": "arora18@gmail.com"
                }
            }
        }
    }
}