# KI Labs Calendar

This Rest API allows anyone to sign up as interviewer or candidate. Further the interviewer or candidate can
create their available time slots using API. 

Also the API provides an option to fetch collection of time slots for interviewer and candidates 
based upon their email addresses.

Summary:

1. Create Users
2. Create Time slots
3. Fetch time slots

For detailed documentation of REST API, please see below link after setup:
http://localhost:8001/api/documentation

## Setup Instructions 

* Clone the docker repo
  ```
  git clone https://rajata07@bitbucket.org/rajata07/kilabsdocker.git
  ```

* Run the following command to start the setup
  ```
  cd kilabsdocker
  chmod +x setup.sh
  ./setup.sh --first
  ```

* Try application by accessing interactive documentation at 
  ```
   http://localhost:8001/api/documentation
  ```
  
####Note: 
In this Implementation, we are using MYSQL as DB. But to scale out the application, an additional
improvement would be to use Memcache and tranfer the data using worker.
