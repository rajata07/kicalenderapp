<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @OAS\Info(
 *     description="KI Labs Calender API documentation",
 *     version="1.0.0",
 *     title="KI Labs Calender API",
 *     termsOfService="http://swagger.io/terms/",
 *     @OAS\Contact(
 *         email="rajata07@gmail.com"
 *     ),
 *     @OAS\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
class Controller extends BaseController
{

}
