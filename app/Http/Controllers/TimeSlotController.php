<?php

namespace App\Http\Controllers;

use App\Services\TimeSlotService;

use Illuminate\Http\Request;

/**
 * Controls the CRUD operation for time slot
 *
 * @package App\Http\Controllers
 */
class TimeSlotController extends Controller
{
    /**
     * @var TimeSlotService Service to handle logic
     */
    private $timeSlotService;

    /** @var Request */
    private $httpRequest;

    /**
     * TimeSlotController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->httpRequest = $request;
        $this->timeSlotService = new TimeSlotService($request);
    }

    /**
     * @OAS\Post(
     *     path="/api/time-slots/",
     *     tags={"Time Slot"},
     *     summary="Save available time slots of given user",
     *     operationId="create",
     *     @OAS\Response(
     *         response=200,
     *         description="successful operation",
     *         @OAS\MediaType(
     *             mediaType="application/json",
     *             @OAS\Schema(
     *                 ref="#/components/schemas/TimeSlot"
     *             )
     *         )
     *     ),
     *     @OAS\RequestBody(
     *         description="Create Time slot object",
     *         required=true,
     *         @OAS\MediaType(
     *             mediaType="application/json",
     *             @OAS\Schema(
     *                 ref="#/components/schemas/TimeSlot"
     *             )
     *         )
     *     )
     * )
     */
    /**
     * Create the time-slot for given user id after performing the required validation.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function create()
    {
        $this->validate($this->httpRequest, [
            'time_slots' => 'required|array',
            'user_id' => 'required|integer'
        ]);

        return $this->timeSlotService->createTimeSlots();
    }

    /**
     * @OAS\Post(
     *     path="/api/time-slots/fetch/",
     *     tags={"Time Slot"},
     *     summary="Fetch a collection of time slots for given users",
     *     operationId="fetch",
     *     @OAS\RequestBody(
     *         @OAS\MediaType(
     *             mediaType="application/json",
     *             @OAS\Schema(
     *                 @OAS\Property(
     *                     property="users",
     *                     type="string"
     *                 ),
     *                 example={
     *                     "users": {"arora18@gmail.com", "rajata07@gmail.com"}
     *                 }
     *             )
     *         )
     *     ),
     *     @OAS\Response(
     *         response=200,
     *         description="successful operation",
     *         @OAS\MediaType(
     *             mediaType="application/json",
     *             @OAS\Schema(
     *                 @OAS\Property(
     *                     property="time_slots",
     *                     type="string"
     *                 ),
     *                 @OAS\Property(
     *                     property="role",
     *                     type="string"
     *                 ),
     *                 @OAS\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OAS\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"time_slots": "", "email": "", "name": "", "role": ""}
     *             )
     *         )
     *     ),
     * )
     */
    /**
     * Fetch collection of time slots for given user id's
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function fetch()
    {
        $this->validate($this->httpRequest, [
            'users' => 'required|array'
        ]);
        return $this->timeSlotService->fetchTimeSlots();
    }
}
