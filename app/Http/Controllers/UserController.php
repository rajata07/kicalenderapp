<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;


class UserController extends Controller
{
    protected $userService;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->userService = new UserService();
    }

    /**
     * @OAS\Get(
     *     path="/api/user/{id}",
     *     tags={"User"},
     *     summary="Fetch user by id",
     *     description="Fetch user data for given id",
     *     operationId="fetch",
     *     @OAS\Response(response=201,
     *          description="In case of successful operation, Response contains user data otherwise corresponding errors are thrown",
     *          @OAS\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OAS\Parameter(
     *         description="ID of user to fetch",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OAS\Schema(
     *             type="integer"
     *         )
     *     ),
     * )
     */
    /**
     * Fetch the user data from DB for given id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetch($id)
    {
        return response()->json(User::find($id));
    }

    /**
     * @OAS\Post(
     *     path="/api/user/",
     *     tags={"User"},
     *     summary="Create user",
     *     description="Simply creates a new user without any auth required",
     *     operationId="createUser",
     *     @OAS\Response(response=201,
     *          description="In case of successful operation, Response contains user data otherwise corresponding errors are thrown",
     *          @OAS\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OAS\RequestBody(
     *         description="Create user object",
     *         required=true,
     *         @OAS\MediaType(
     *             mediaType="application/json",
     *             @OAS\Schema(
     *                 ref="#/components/schemas/User"
     *             )
     *         )
     *     )
     * )
     */
    /**
     * Validate the parameters and create the user.
     *
     * @param Request $request Request Obkect
     * @return \Illuminate\Http\JsonResponse Json response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'nullable|string',
            'email' => 'required|email|unique:user',
            'role' => 'required|string'
        ]);
        return $this->userService->create($request);
    }

    /**
     * Update the given user
     * @TODO implementation required
     * @param integer $id User id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return response()->json(['operation not supported']);
    }

    /**
     * @OAS\Delete(
     *     path="/api/user/{id}",
     *     tags={"User"},
     *     summary="Delete user",
     *     description="Removes the user data.",
     *     operationId="delete",
     *     @OAS\Parameter(
     *         name="id",
     *         in="path",
     *         description="The user id that needs to be deleted",
     *         required=true,
     *         @OAS\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OAS\Response(
     *         response=204,
     *         description="User deleted"
     *     ),
     *     @OAS\Response(
     *         response=404,
     *         description="User not found",
     *     )
     * )
     */
    /**
     * Delete the user
     *
     * @param integer $id User Id
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function delete($id)
    {
        return $this->userService->delete($id);
    }
}
