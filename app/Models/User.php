<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OAS\Schema(
 *   schema="User",
 *   type="object",
 *   required={"email", "role"},
 *   @OAS\Property(
 *       property="role",
 *       description="Role of the user (interviewer or candidate)",
 *       type="string",
 *   ),
 *   @OAS\Property(
 *       property="name",
 *       description="Name the user (optional)",
 *       type="string",
 *   ),
 *   @OAS\Property(
 *       property="email",
 *       description="Email of the user",
 *       type="string",
 *   ),
 *   example={"role": "interviewer", "name": "Rajat", "email": "arora18@gmail.com"}
 * )
 */
class User extends Model
{
    /**
     * override table name
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function roles()
    {
        return $this->hasOne(Roles::class, 'foreign_key');
    }
}
