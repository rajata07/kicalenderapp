<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    const ALLOWED_ROLES = [
        'interviewer',
        'candidate'
    ];
}
