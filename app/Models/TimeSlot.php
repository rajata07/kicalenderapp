<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @OAS\Schema(
 *   schema="TimeSlot",
 *   type="object",
 *   required={"user_id", "time_slots"},
 *   @OAS\Property(
 *       property="user_id",
 *       description="(Id of the user)",
 *       type="integer",
 *   ),
 *   @OAS\Property(
 *       property="time_slots",
 *       description="(Slots available for user as json)",
 *       type="string",
 *   ),
 *   @OAS\Property(
 *       property="email",
 *       description="Email of the user",
 *       type="string",
 *   ),
 *  example=
 *      {
 *       "user_id": 10,
 *       "time_slots": {
 *           {"start_time": "2018-05-20 09:00", "end_time": "2018-05-20 16:00"},
 *           {"start_time": "2018-05-20 10:00", "end_time": "2018-05-20 11:00"}
 *      }}
 *     )
 */
class TimeSlot extends Model
{
    /**
     * @var integer
     */
    public $user_id;

    /**
     * @var array
     */
    public $time_slots;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id', 'time_slots'
    ];
}
