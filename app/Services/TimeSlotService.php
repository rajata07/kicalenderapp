<?php

declare(strict_types=1);

/**
 * service for time slot functionality (CRUD operation and format validation)
 * @author Rajat Arora <rajata07@gmail.com>
 */

namespace App\Services;

use App\Models\User;
use DateInterval;
use DateTime;
use App\Models\TimeSlot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;

class TimeSlotService
{
    /** @var string Restrict to this date format for now for time slots. */
    const DATE_TIME_FORMAT = 'Y-m-d H:i';

    /**
     * @var Request
     */
    protected $httpRequest;

    /**
     * TimeSlotService constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->httpRequest = $request;
    }

    /**
     * Create time slots for given user id.
     *
     * @TODO Validate if time slots are not in past
     * @TODO Write unit tests
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function createTimeSlots()
    {
        $userId = $this->httpRequest->get('user_id');
        $timeSlots = $this->httpRequest->get('time_slots');

        //check if user exists
        if (!$this->userExists($userId)) {
            return response()->json(["user_id" => 'user not found'], 404);
        }

        //Validate and split time-slots
        $slots = [];
        foreach ($timeSlots as $timeSlot) {
            if (!isset($timeSlot['start_time'], $timeSlot['end_time'])) {
                return response()->json(["time_slots" => ["missing start_time or end_time in time slot"]], 422);
            }

            $isValidStartTime = $this->validateDate($timeSlot['start_time']);
            $isValidEndTime = $this->validateDate($timeSlot['end_time']);

            if (!$isValidStartTime || !$isValidEndTime) {
                return response()->json([
                    "time_slots" => [
                        "invalid format for time slot. Allowed format is " . self::DATE_TIME_FORMAT,
                        "only hour format is allowed for date time, example 09:00 is allowed and 09:30 is not allowed"
                    ]
                ], 422);
            }

            $startTime = new DateTime($timeSlot['start_time']);
            $endTime = new DateTime($timeSlot['end_time']);
            $time = $startTime;

            //split chunks
            while ($time < $endTime) {
                $startTimeChunk = $time->format(self::DATE_TIME_FORMAT);
                $interval = $time->add(new DateInterval('PT1H'));
                $endTimeChunk = $interval->format(self::DATE_TIME_FORMAT);
                $formattedTimeSlot = "{$startTimeChunk}-{$endTimeChunk}";

                //hash is just in case, we need to book certain slot in future, we can search by hash
                $hash = md5($userId . $formattedTimeSlot);
                $slots[$hash] = $formattedTimeSlot;
            }
        }

        //Check existing time slots
        $existingTimeSlots = TimeSlot::where('user_id', $userId)->first();
        if (!empty($existingTimeSlots['time_slots'])) {
            $existingTimeSlotDecoded = json_decode($existingTimeSlots['time_slots'], true);
            foreach ($slots as $hash => $slot) {
                if (!empty($existingTimeSlotDecoded[$hash])) {
                    unset($slots[$hash]);
                }
            }
            $existingTimeSlots->update([
                'time_slots' => json_encode(array_merge($existingTimeSlotDecoded, $slots))
            ]);
        } else {
            //finally create the time slots in JSON format
            TimeSlot::create([
                'user_id' => $userId,
                'time_slots' => json_encode($slots)
            ]);
        }

        return response()->json("success", 201);
    }

    /**
     * Fetch time slot collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchTimeSlots()
    {
        $userEmails = $this->httpRequest->get('users');
        $users = DB::table('time_slots')
            ->join('user', 'user.id', '=', 'time_slots.user_id')
            ->join('role_user', 'time_slots.user_id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('user.name', 'user.email', 'time_slots.time_slots', 'roles.name AS role')
            ->whereIn('user.email', $userEmails)
            ->get();

        return response()->json($users, 201);
    }

    /**
     * Check if the user exists for given id
     * @param integer $id User id
     * @return bool
     */
    protected function userExists(int $id): bool
    {
        $user = User::find($id);
        return empty($user['id']) ? false : true;
    }

    /**
     * Validate the date time format. We allow only complete hours like 09:00 and for example 09:30 is not allowed
     * @TODO Write unit tests
     *
     * @param string $date Date to be validated
     * @param string $format format in which date is allowed
     *
     * @return bool|false|int
     */
    protected function validateDate($date, $format = self::DATE_TIME_FORMAT)
    {
        $dateWithformat = DateTime::createFromFormat($format, $date);
        $isValidFormat = ($dateWithformat && $dateWithformat->format($format) == $date);

        if ($isValidFormat) {
            $time = $dateWithformat->format("H:i");
            $isValidFormat = preg_match('/^\d\d:00/', $time);
        }
        return $isValidFormat;
    }
}
