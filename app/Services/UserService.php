<?php
/**
 * Service for handling user CRUD operations
 * @author Rajat Arora <rajata07@gmail.com>
 */
namespace App\Services;

use App\Models\Roles;
use App\Models\RoleUser;
use App\Models\User;

class UserService
{
    /**
     * Validates the request and create the user
     * @param \Illuminate\Http\Request $request Request object which contains all the http parameters
     * @return \Illuminate\Http\JsonResponse    JSON response
     * @throws \Exception
     * @TODO Write unit tests
     */
    public function create($request)
    {
        if (!in_array($request->get('role'), Roles::ALLOWED_ROLES)) {
            return response()->json(
                ["role" => "invalid role specified. Allowed values are " . implode(',', Roles::ALLOWED_ROLES)],
                422
            );
        }

        $user = User::create($request->all());
        if (!empty($user['id'])) {
            try {
                $role = Roles::where(['name' => $request->get('role')])->first();
                RoleUser::create([
                    'user_id' => $user['id'],
                    'role_id' => $role['id']
                ]);
            } catch (\Exception $e) {
                //delete the user as it was not successful creation
                User::findOrFail($user['id'])->delete();
                throw new \Exception($e);
            }
        }

        return response()->json($user, 201);
    }

    /**
     * Delete the user with given id
     * @param integer $id User Id
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @TODO Write unit tests
     */
    public function delete($id)
    {
        try {
            User::findOrFail($id)->delete();
        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }
        return response('Deleted Successfully', 200);
    }
}
