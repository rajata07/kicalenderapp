<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\User;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    //CRUD User
    $router->get('user/{id}', ['uses' => 'UserController@fetch']);

    $router->post('user', ['uses' => 'UserController@create']);

    $router->delete('user/{id}', ['uses' => 'UserController@delete']);

    $router->put('user/{id}', ['uses' => 'UserController@update']);


    //CRUD time-slots
    $router->post('time-slots/fetch/',  ['uses' => 'TimeSlotController@fetch']);

    $router->post('time-slots', ['uses' => 'TimeSlotController@create']);

    $router->delete('time-slots/{userId}', ['uses' => 'TimeSlotController@delete']);

    $router->put('users/{id}', ['uses' => 'TimeSlotController@update']);
});
