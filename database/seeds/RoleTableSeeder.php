<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Roles::ALLOWED_ROLES as $role) {
            DB::table('roles')->insert([
                'name' => $role
            ]);
        }
    }
}
